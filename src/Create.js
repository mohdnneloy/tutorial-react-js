import {useState} from 'react';
import {useHistory} from 'react-router-dom';
const Create = () => {

    const [title,setTitle] = useState('');
    const [body,setBody] = useState('');
    const [author,setAuthor] = useState('yoshi');
    const [isPending, setIsPending] = useState(false);
    const history = useHistory();
    
    const handleSubmit = (e) => {
        e.preventDefault(); // Prevents page from refreshing on click on Button
        const blog = {title, body, author};

        setIsPending(true);
        
        fetch('http://localhost:8000/blogs',{ // Second parameter
            method: 'POST',
            headers: {"Content-Type": "application/json"}, // Json Data on the Post Request
            body: JSON.stringify(blog)
        }).then (() => {
            console.log('new blog added');
            setIsPending(false);
            //history.go(-1); // Go back once its like clicking the back button on the browser
            history.push('/'); // Redirect to a route
        })


    }

    return (  
        <div className="create">
            <h2>Add new Blogs</h2>
            <form onSubmit = {handleSubmit}>
                <label >Blog Title: </label>
                <input type='text' required 
                value = {title}
                onChange = {(e) => setTitle(e.target.value)} /> {/*onChange event if any change is made*/}
                <label >Blog Body: </label>
                <textarea 
                    required
                    onChange = {(e) => setBody(e.target.value) }
                ></textarea>
                <label >Blog Author: </label>
                <select value = {author} onChange = {(e) => setAuthor(e.target.value)} >
                    <option value="mario">mario</option>
                    <option value="yoshi">yoshi</option>
                </select>
                {!isPending && <button>Add a Blog</button>}
                {isPending && <button>Add a Blog...</button>}
                <p>{title}</p>
                <p>{body}</p>
                <p>{author}</p>
            </form>
        </div>
    );
}
 
export default Create;
<div className="create">
    <h2>Add new Blogs</h2>
</div>