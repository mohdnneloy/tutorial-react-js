import Navbar from './Navbar.js';
import Home from './Home.js';
import Create from './Create.js'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import BlogDetails from './BlogDetails.js';
import NotFound from './NotFound.js';

function App() { // Single component

  return (

    <Router>
      <div className="App">
        <Navbar />
        <div className="content">
          <Switch>
            <Route exact path = "/"> {/* 'Exact' is required here,  Otherwise the forward Slash will be considered same in all route*/}
              <Home />
            </Route>
            <Route path = "/create">
              <Create />
            </Route>
            <Route path = "/blogs/:id">
              <BlogDetails />
            </Route>
            <Route path = "*"> {/* '*' means any other route */}
              <NotFound />
            </Route>
          </Switch>
        </div>
      </div>
    </Router>

  );
}

export default App;
